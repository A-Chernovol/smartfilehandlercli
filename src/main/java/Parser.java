import java.io.File;
import java.util.List;

public interface Parser {
    List<String> getSupportedExtensions();

    boolean isSupportFile(String str);

    List<ResultItem> parse(File file, String str);
}
