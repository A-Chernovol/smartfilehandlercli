import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TxtParser extends AbstractParser {
    private final FileReader fileReader = new FileReader();

    public TxtParser() {
        super(List.of(".txt"));
    }

    @Override
    public List<ResultItem> parse(File file, String str) {
        List<ResultItem> tempList = new ArrayList<>();
        try {
            List<String> lines = fileReader.readLines(file);
            for (int i = 0; i < lines.size(); i++) {
                if (lines.get(i).contains(str)) {
                    tempList.add(new SimpleResultItem(lines.get(i), i + 1));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Reading error");
        }
        return tempList;
    }
}
