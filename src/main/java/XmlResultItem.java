public class XmlResultItem implements ResultItem {
    private final String node;
    private final String value;

    public XmlResultItem(String node, String value) {
        this.node = node;
        this.value = value;
    }

    @Override
    public String getFormatted() {
        return "Tag : <" + node + "> : " + value;
    }
}
