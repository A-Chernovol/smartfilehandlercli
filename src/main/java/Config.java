import java.io.File;

public class Config {
    private File destination;
    private String text4Search;

    public Config(String[] args) {
        parsArgs(args);
    }

    private void parsArgs(String[] args) {
        if (args.length == 0) {
            throw new ConfigException("No args");
        }
        if (args[args.length - 1] != null) {
            destination = new File(args[args.length - 1]);
            if (!destination.exists()) {
                throw new ConfigException("File doesn't exist...");
            }
        } else {
            throw new ConfigException("Incorrect path...");
        }
        if (args[args.length - 2] != null && !args[args.length - 2].equals("")) {
            text4Search = args[args.length - 2];
        } else {
            throw new ConfigException("Incorrect data for search...");
        }
    }

    public File getDestination() {
        return destination;
    }

    public String getText4Search() {
        return text4Search;
    }
}
