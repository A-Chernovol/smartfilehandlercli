import java.util.List;

abstract public class AbstractParser implements Parser {
    private final List<String> supportedExtensions;

    protected AbstractParser(List<String> supportedExtensions) {
        this.supportedExtensions = supportedExtensions;
    }

    @Override
    public List<String> getSupportedExtensions() {
        return supportedExtensions;
    }

    @Override
    public boolean isSupportFile(String str) {
        return supportedExtensions.contains(str);
    }
}
