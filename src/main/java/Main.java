public class Main {
    public static void main(String[] args) {
        Dispatcher dispatcher = new Dispatcher(args);
        dispatcher.dispatch();
    }
}
