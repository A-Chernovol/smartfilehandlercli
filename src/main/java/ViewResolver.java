import java.io.File;
import java.util.List;
import java.util.Map;

public class ViewResolver {

    public String resolve(Result result, String str) {
        StringBuilder sb = new StringBuilder();
        Map<File, List<ResultItem>> res = result.getMatches();
        sb.append("==============================\n")
                .append("Smart File Handler CLI\n")
                .append("==============================\n")
                .append("Result for : ")
                .append(str)
                .append('\n');
        for (Map.Entry<File, List<ResultItem>> entry : res.entrySet()) {
            if (entry.getValue().isEmpty()) {
                continue;
            }
            sb.append("File : ")
                    .append(entry.getKey())
                    .append("; Total matches : ")
                    .append(entry.getValue().size())
                    .append('\n');
            for (int i = 0; i < entry.getValue().size(); i++) {
                sb.append('\t').append(entry.getValue().get(i).getFormatted()).append('\n');
            }
        }
        sb.append("==============================\n");
        return sb.toString();
    }
}
