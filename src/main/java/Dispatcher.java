import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Dispatcher {
    private final Config config;
    private final Handler handler;
    private final List<View> views;
    private final ViewResolver viewResolver;

    public Dispatcher(String[] args) {
        config = new Config(args);
        handler = new Handler();
        views = new ArrayList<>();
        viewResolver = new ViewResolver();
    }

    public void dispatch() {
        Result result = new Result();
        if (config.getDestination().isFile()) {
            result.addMatches(config.getDestination(), handler.handle(config.getDestination(), config.getText4Search()));
        } else {
            File[] files = config.getDestination().listFiles();
            assert files != null;
            for (File f : files) {
                if (f.isFile()) {
                    result.addMatches(f, handler.handle(f, config.getText4Search()));
                }
            }
        }
        views.add(new ConsoleView());
        for (View view : views){
            view.print(viewResolver.resolve(result, config.getText4Search()));
        }

    }
}
