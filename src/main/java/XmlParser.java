import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class XmlParser extends AbstractParser {
    public XmlParser() {
        super(List.of(".xml"));
    }

    @Override
    public List<ResultItem> parse(File file, String str) {
        List<ResultItem> list = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(file);
            doc.normalize();
            parseRec(list, doc.getElementsByTagName(doc.getDocumentElement().getTagName()), str);
        } catch (Exception e) {
            throw new ConfigException("Error...");
        }
        return list;
    }

    private void parseRec(List<ResultItem> list, NodeList nodeList, String str) {
        for (int i = 0; i < nodeList.getLength(); i++){
            if(nodeList.item(i).hasChildNodes()){
                parseRec(list,nodeList.item(i).getChildNodes(),str);
            } else {
                if(nodeList.item(i).getTextContent().contains(str)){
                    list.add(new XmlResultItem(nodeList.item(i).getParentNode().getNodeName(),nodeList.item(i).getTextContent()));
                }
            }
        }
    }
}
