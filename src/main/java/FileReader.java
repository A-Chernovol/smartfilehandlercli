import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileReader {
    public List<String> readLines(File file) throws IOException {
        List<String> list = new ArrayList<>();
        BufferedReader br = new BufferedReader(new java.io.FileReader(file));
        String str;
        while ((str = br.readLine()) != null) {
            list.add(str);
        }
        return list;
    }
}
