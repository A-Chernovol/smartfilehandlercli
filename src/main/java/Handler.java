import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Handler {
    private final List<Parser> parsers = new ArrayList<>();

    public Handler() {
        parsers.add(new TxtParser());
        parsers.add(new XmlParser());
    }

    public List<ResultItem> handle(File file, String str) {
        for (Parser p : parsers) {
            if (p.isSupportFile(file.getName().substring(file.getName().lastIndexOf('.')))) {
                return p.parse(file, str);
            }
        }
        return new ArrayList<>();
    }
}
